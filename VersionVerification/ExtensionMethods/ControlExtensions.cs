﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace VersionVerification.Core.ExtensionMethods
{
    public static class ControlExtensions
    {
        public static UITestControl FindControlByName(this UITestControlCollection collection, string name)
        {
            return collection.FirstOrDefault(c => c.Name == name);
        }
    }
}