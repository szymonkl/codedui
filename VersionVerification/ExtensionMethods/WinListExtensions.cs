﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VersionVerification.Core.ExtensionMethods
{
    public static class WinListExtensions
    {
        private const int MaxNumberOfScrolls = 30;
        private const int ScrollInterval = 5;
        private enum Direction
        {
            Up = 1,
            Down = -1
        }

        public static WinListItem FindItem(this WinList list, string itemName, bool scrollingEnabled = true)
        {
            return scrollingEnabled ? FindItemWithScrolling(list, itemName) : FindItemWithoutScrolling(list, itemName);
        }

        private static WinListItem FindItemWithScrolling(WinList list, string itemName)
        {
            var item = TryFindItemWithScrolling(list, itemName, Direction.Down);
            if (item == null)
            {
                Mouse.MoveScrollWheel(MaxNumberOfScrolls * ScrollInterval * (int)Direction.Up);
                item = TryFindItemWithScrolling(list, itemName, Direction.Up);
            }
            if (item == null)
            {
                Assert.Fail($"Cannot select item {itemName}, item doesn't exist");
            }

            return item;
        }

        private static WinListItem TryFindItemWithScrolling(WinList list, string itemName, Direction scrollingDirection)
        {
            WinListItem item = null;
            for (var scrollAttempt = 0; scrollAttempt < MaxNumberOfScrolls; scrollAttempt++)
            {
                item = list.Items.FindControlByName(itemName) as WinListItem;
                if (item == null)
                {
                    Mouse.MoveScrollWheel(list, (int)scrollingDirection * ScrollInterval);
                }
                else
                {
                    break;
                }
            }

            return item;
        }

        public static WinListItem FindItemWithoutScrolling(this WinList list, string itemName)
        {
            var item = list.Items.FindControlByName(itemName);
            if (item == null)
            {
                Assert.Fail($"Cannot select item {itemName}, item doesn't exist");
            }

            return (WinListItem) item;
        }
    }
}
