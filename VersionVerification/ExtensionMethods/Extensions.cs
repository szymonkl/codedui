﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VersionVerification.Core.ExtensionMethods
{
    public static class Extensions
    {
        public static List<T> RemoveFirst<T>(this List<T> list)
        {
            list.Remove(list.FirstOrDefault());
            return list;
        }

        public static List<T> RemoveLast<T>(this List<T> list)
        {
            list.Remove(list.LastOrDefault());
            return list;
        }

        public static List<T> RemoveFirstAndLast<T>(this List<T> list)
        {
            RemoveFirst(list);
            RemoveLast(list);
            return list;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));

            foreach (T item in source)
            {
                action(item);
            }
        }

        public static string AddQuotations(this string input) => $"\"{input}\"";
        
    }
}
