﻿using Microsoft.VisualStudio.TestTools.UITesting;

namespace VersionVerification.Core.Engine
{
    public interface IWindowsController
    {
        /// <summary>
        /// Finds opened windows used for testing
        /// </summary>
        /// <returns>Collection of opened windows</returns>
        UITestControlCollection FindOpenedWindows();
    }
}