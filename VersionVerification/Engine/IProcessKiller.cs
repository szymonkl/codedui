﻿namespace VersionVerification.Core.Engine
{
    public interface IProcessKiller
    {
        /// <summary>
        /// Kills processes with specified name
        /// </summary>
        /// <param name="processName">Process name</param>
        void KillProcess(string processName);
    }
}