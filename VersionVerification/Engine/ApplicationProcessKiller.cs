﻿using System.Diagnostics;
using System.Linq;
using VersionVerification.Core.ExtensionMethods;

namespace VersionVerification.Core.Engine
{
    public class ApplicationProcessKiller : IProcessKiller
    {
        public void KillProcess(string processName)
        {
            Process.GetProcessesByName(processName).Where(p => !p.HasExited).ForEach(p => p.Kill());
        }
    }
}
