﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using VersionVerification.Core.ExtensionMethods;
using VersionVerification.Core.Properties;

namespace VersionVerification.Core.Engine
{
    public class WindowsController : IWindowsController
    {
        public UITestControlCollection FindOpenedWindows()
        {
            var openedWindows = new UITestControlCollection();
            FindOpenedPropertiesWindows().ForEach(window => openedWindows.Add(window));
            FindOpenedFileExplorerWindows().ForEach(window => openedWindows.Add(window));
            //FindOpenedWindowsCannotFindErrorWindows().ForEach(window => openedWindows.Add(window));
            //FindOpenedLocationIsUnavailableWindows().ForEach(window => openedWindows.Add(window));
            //FindOpenedRunWindows().ForEach(window => openedWindows.Add(window));
            
            return openedWindows;
        }

        private static UITestControlCollection FindOpenedPropertiesWindows()
        {
            var control = new UITestControl { TechnologyName = "MSAA" };
            control.SearchProperties.Add(UITestControl.PropertyNames.Name, Resources.Properties, PropertyExpressionOperator.Contains);
            control.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
            control.SearchProperties.Add(UITestControl.PropertyNames.ClassName, "#32770");
            return control.FindMatchingControls();
        }

        private static UITestControlCollection FindOpenedFileExplorerWindows()
        {
            var control = new UITestControl { TechnologyName = "MSAA" };
            control.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
            control.SearchProperties.Add(UITestControl.PropertyNames.ClassName, "CabinetWClass");
            return control.FindMatchingControls();
        }

        private static UITestControlCollection FindOpenedWindowsCannotFindErrorWindows()
        {
            var control = new UITestControl { TechnologyName = "MSAA" };
            control.SearchProperties.Add(UITestControl.PropertyNames.ClassName, "#32770");
            control.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
            var childControl = new WinText(control);
            childControl.SearchProperties.Add(UITestControl.PropertyNames.Name, Resources.WindowsCannotFind, PropertyExpressionOperator.Contains);
            var windows = new UITestControlCollection();
            childControl.FindMatchingControls().ForEach(c => windows.Add(c.TopParent));
            return windows;
        }

        private static UITestControlCollection FindOpenedLocationIsUnavailableWindows()
        {
            var control = new UITestControl {TechnologyName = "MSAA"};
            control.SearchProperties.Add(UITestControl.PropertyNames.Name, Resources.LocationIsUnavailable);
            control.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
            return control.FindMatchingControls();
        }

        private static UITestControlCollection FindOpenedRunWindows()
        {
            var control = new UITestControl { TechnologyName = "MSAA" };
            control.SearchProperties.Add(UITestControl.PropertyNames.Name, Resources.Run);
            control.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
            control.SearchProperties.Add(UITestControl.PropertyNames.ClassName, "#32770");
            return control.FindMatchingControls();
        }
    }
}