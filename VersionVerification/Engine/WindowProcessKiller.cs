﻿using System.Diagnostics;
using System.Linq;
using VersionVerification.Core.ExtensionMethods;

namespace VersionVerification.Core.Engine
{
    public class WindowProcessKiller : IProcessKiller
    {
        public void KillProcess(string processName)
        {
            var processes = Process.GetProcessesByName(processName).Where(p => !p.HasExited && !string.IsNullOrEmpty(p.MainWindowTitle));
            processes.ForEach(p => p.Kill());
        }
    }
}