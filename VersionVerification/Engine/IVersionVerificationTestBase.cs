﻿namespace VersionVerification.Core.Engine
{
    public interface IVersionVerificationTestBase
    {
        /// <summary>
        /// Object which is used for test initialization
        /// </summary>
        ITestInitializer TestInitializer { get; }

        /// <summary>
        /// Method which is run at the beggining of every single test
        /// </summary>
        void TestInitialize();

        /// <summary>
        /// Method which is run at the end of every single test
        /// </summary>
        void TestCleanup();
    }
}