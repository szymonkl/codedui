﻿namespace VersionVerification.Core.Engine
{
    public interface ITestInitializer
    {
        /// <summary>
        /// Method which closes opened windows
        /// </summary>
        void CleanupScreen();
        /// <summary>
        /// Method which initializes playback settings
        /// </summary>
        void InitializePlayback();
    }
}