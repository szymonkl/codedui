﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VersionVerification.Core.Engine
{
    public class VersionVerificationTestBase : IVersionVerificationTestBase
    {
        public ITestInitializer TestInitializer => new TestInitializer();
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInitialize()
        {
            TestInitializer.InitializePlayback();
            TestInitializer.CleanupScreen();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            TestInitializer.CleanupScreen();
        }
    }
}
