﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using VersionVerification.Core.ExtensionMethods;

namespace VersionVerification.Core.Engine
{
    public class TestInitializer : ITestInitializer
    {
        private static readonly IProcessKiller WindowProcessKiller = new WindowProcessKiller();
        private static readonly IProcessKiller ApplicationProcessKiller = new ApplicationProcessKiller();
        private readonly Dictionary<string, IProcessKiller> applicationProcesses = new Dictionary<string, IProcessKiller>
        {
            {"explorer", WindowProcessKiller},
            {"RMDeviceMonitor", ApplicationProcessKiller},
            {"ExecutorConfigUI.JP", ApplicationProcessKiller},
            {"RMServerUtility", ApplicationProcessKiller},
            {"Motorola.CommonCPS.RadioManagement.Shell", ApplicationProcessKiller},
            {"APXFamilyCPS", ApplicationProcessKiller},
            {"ApxFamilyTuner", ApplicationProcessKiller}
        };

        public void CleanupScreen()
        {
            try
            {
                this.applicationProcesses.ForEach(p => p.Value.KillProcess(p.Key));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        public void InitializePlayback()
        {
            if (!Playback.IsInitialized)
                Playback.Initialize();

            Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.Disabled;
            Playback.PlaybackSettings.MaximumRetryCount = 10;
            Playback.PlaybackSettings.ShouldSearchFailFast = false;
            Playback.PlaybackSettings.DelayBetweenActions = 600;

#if DEBUG
            Playback.PlaybackSettings.SearchTimeout = 10000;
#else
            Playback.PlaybackSettings.SearchTimeout = 15000;
#endif
        }

    }
}