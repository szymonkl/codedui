﻿using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using VersionVerification.Core.Views.FileProperties.Tabs;

namespace VersionVerification.Core.Views.FileProperties
{
    public interface IFilePropertiesWindow
    {
        /// <summary>
        /// Gets the window instance (for internal framework purposes)
        /// </summary>
        WinWindow Window { get; }

        /// <summary>
        /// Opens specified property tab
        /// </summary>
        /// <typeparam name="T">Property tab (implementing IPropertyTab)</typeparam>
        /// <returns>Property tab</returns>
        T OpenPropertyTab<T>() where T : IPropertyTab, new();

        /// <summary>
        /// Closes current File Properties window
        /// </summary>
        void CloseWindow();

    }
}