﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using VersionVerification.Core.Properties;
using VersionVerification.Core.Views.FileProperties.Tabs;

namespace VersionVerification.Core.Views.FileProperties
{
    public class FilePropertiesWindow : WindowBase, IFilePropertiesWindow
    {
        public override WinWindow Window
        {
            get
            {
                if (this.window == null)
                {
                    this.window = new WinWindow();
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.Name, Resources.Properties, PropertyExpressionOperator.Contains);
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.ClassName, "#32770");
                    
                }
                return this.window;
            }
        }

        private WinWindow window;

        public T OpenPropertyTab<T>() where T : IPropertyTab, new ()
        {
            T propertyTabView = new T {ParentWindow = this};
            var tab = GetPropertyTab(propertyTabView.Name);
            tab.SetFocus();
            Mouse.Click(tab);
            return propertyTabView;
        }

        private WinTabPage GetPropertyTab(string name)
        {
            var tab = new WinTabPage(Window);
            tab.SearchProperties.Add(UITestControl.PropertyNames.Name, name);
            tab.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "TabPage");
            return tab;
        }
    }
}