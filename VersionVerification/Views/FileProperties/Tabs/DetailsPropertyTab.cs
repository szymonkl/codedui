﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Properties;

namespace VersionVerification.Core.Views.FileProperties.Tabs
{
    public class DetailsPropertyTab : IPropertyTab
    {
        public string Name => Resources.Details;

        public WinList ItemList
        {
            get
            {
                if (this.itemList == null)
                {
                    this.itemList = new WinList(ParentWindow.Window);
                    this.itemList.SearchProperties.Add(UITestControl.PropertyNames.ClassName, "SysListView32");
                    this.itemList.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "List");
                }
                return this.itemList;
            }
            
        }

        public IFilePropertiesWindow ParentWindow { get; set; }

        private WinList itemList;

        public DetailsPropertyTab VerifyProductName(string expectedFileName)
        {
            var fileName = GetProductName();
            Assert.AreEqual(expectedFileName, fileName, $"Expected file was {expectedFileName}, but found {fileName}");

            return this;
        }

        public DetailsPropertyTab VerifyFileVersion(string expectedVersion)
        {
            var fileName = GetProductName();
            var currentFileVersion = GetFileVersion();
            Assert.AreEqual(expectedVersion, currentFileVersion, $"Expected file version for {fileName} was {expectedVersion}, but found {currentFileVersion}");

            return this;
        }

        private string GetProductName()
        {
            var listItem = GetListItem(Resources.ProductName);
            return listItem.AccessibleDescription.Replace(Resources.Value + ": ", string.Empty).Trim();
        }

        private WinListItem GetListItem(string name)
        {
            var listItem = new WinListItem(ItemList);
            listItem.SearchProperties.Add(UITestControl.PropertyNames.Name, name);
            listItem.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "ListItem");
            return listItem;
        }

        private string GetFileVersion()
        {
            var listItem = GetListItem(Resources.FileVersion);
            return listItem.AccessibleDescription.Replace(Resources.Value + ": ", string.Empty).Trim();
        }

        public DetailsPropertyTab VerifyProductVersion(string expectedVersion)
        {
            var fileName = GetProductName();
            var currentProductVersion = GetProductVersion();
            Assert.AreEqual(expectedVersion, currentProductVersion, $"Expected product version for {fileName} was {expectedVersion}, but found {currentProductVersion}");

            return this;
        }

        private string GetProductVersion()
        {
            var listItem = GetListItem(Resources.ProductVersion);
            return listItem.AccessibleDescription.Replace(Resources.Value + ": ", string.Empty).Trim();
        }

        public DetailsPropertyTab VerifyCopyrightInfo(string expectedCopyright)
        {
            var fileName = GetProductName();
            var currentCopyrightInfo = GetCopyrightInfo();
            Assert.AreEqual(expectedCopyright, currentCopyrightInfo, $"Expected copyright info for {fileName} was {expectedCopyright}, but found {currentCopyrightInfo}");

            return this;
        }

        private string GetCopyrightInfo()
        {
            var listItem = GetListItem(Resources.Copyright);
            return listItem.AccessibleDescription.Replace(Resources.Value + ": ", string.Empty).Trim();
        }

        public void CloseWindow()
        {
            ParentWindow.CloseWindow();
        }
    }
}