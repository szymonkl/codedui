﻿namespace VersionVerification.Core.Views.FileProperties.Tabs
{
    public interface IPropertyTab
    {
        /// <summary>
        /// Gets the name of the property tab
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets the parent File Properties window
        /// </summary>
        IFilePropertiesWindow ParentWindow { get; set; }

        /// <summary>
        /// Closes current File Properties window
        /// </summary>
        void CloseWindow();
    }
}