﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.ExtensionMethods;
using VersionVerification.Core.Properties;
using VersionVerification.Core.Views.ListItem;

namespace VersionVerification.Core.Views.FileExplorer
{
    public class FileExplorerWindow : WindowBase, IStartupView, IFileExplorerWindow
    {
        public string Name { get; }

        public override WinWindow Window
        {
            get
            {
                if (this.fileExplorer == null)
                {
                    this.fileExplorer = new WinWindow();
                    this.fileExplorer.SearchProperties.Add(UITestControl.PropertyNames.Name, Name);
                    this.fileExplorer.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }
               
                this.fileExplorer.SetFocus();
                return this.fileExplorer;
            }
        }

        public WinList FolderList
        {
            get
            {
                if (this.winList == null)
                {
                    this.winList = new WinList(Window);
                    this.winList.SearchProperties.Add(UITestControl.PropertyNames.Name, Resources.ItemsView);
                    this.winList.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "List");
                }
                return this.winList;
            }
        }

        private WinWindow fileExplorer;
        private WinList winList;

        public FileExplorerWindow()
        {
            StartProcess(string.Empty);
        }

        public FileExplorerWindow(string path)
        {
            Name = Path.GetFileNameWithoutExtension(path);
            StartProcess(path);
        }

        private static void StartProcess(string args)
        {
            var process = new Process();
            var startInfo = new ProcessStartInfo("explorer.exe") {Arguments = args};
            process.StartInfo = startInfo;
            process.Start();
        }

        public IFileExplorerWindow VerifyFolderExists(string folderName, bool expected = true)
        {
            var folder = FindItem(folderName);
            var exists = folder != null && folder.Exists;
            var errorMessage = expected
                ? $"File or folder {folderName} doesn't exist, but it should"
                : $"File or folder {folderName} exists, but it shouldn't";
            Assert.AreEqual(expected, exists, errorMessage);

            return this;
        }

        private WinListItem FindItem(string itemName) => FolderList.FindItem(itemName);

        public IFileExplorerWindow VerifyPathExists(params string[] path)
        {
            path.ToList().RemoveLast().ForEach(s =>
            {
                VerifyFolderExists(s);
                OpenFolder(s);
            });

            VerifyFolderExists(path.Last());

            return this;
        }

        public IFileExplorerWindow OpenFolder(string folderName)
        {
            Mouse.DoubleClick(FindItem(folderName));
            return this;
        }

        public IListItemView SelectItem(string name)
        {
            var folder = FindItem(name);
            Mouse.Click(folder);
            return new ListItemView(folder);
        }

        public IFileExplorerWindow BackToPreviousPath()
        {
            Keyboard.SendKeys(Window, "%{LEFT}");
            return this;
        }

        public IFileExplorerWindow VerifyFilesAmount(int expected)
        {
            var current = FolderList.Items.Count;
            Assert.AreEqual(expected, current, $"Expected files or folders amount is {expected}, but was {current}.");
            return this;
        }

        public IFileExplorerWindow VerifyFilesUnique()
        {
            CollectionAssert.AllItemsAreUnique(FolderList.Items.GetNamesOfControls());
            return this;
        }
    }
}