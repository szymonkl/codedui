﻿using VersionVerification.Core.Views.ListItem;

namespace VersionVerification.Core.Views.FileExplorer
{
    public interface IFileExplorerWindow
    {
        /// <summary>
        /// Selects item with specified name
        /// </summary>
        /// <param name="name">Name of folder or file</param>
        /// <returns>List Item view</returns>
        IListItemView SelectItem(string name);

        /// <summary>
        /// Opens foleder with specified name
        /// </summary>
        /// <param name="folderName">Name of the folder to be opened</param>
        /// <returns>File Explorer window</returns>
        IFileExplorerWindow OpenFolder(string folderName);

        /// <summary>
        /// Goes to previously opened path
        /// </summary>
        /// <returns>File Explorer window</returns>
        IFileExplorerWindow BackToPreviousPath();

        /// <summary>
        /// Verifies if folder with specified name exists in the location opened in file explorer
        /// </summary>
        /// <param name="folderName">Name of the folder</param>
        /// <param name="expected">Determines if folder should exist or not</param>
        /// <returns>File Explorer window</returns>
        IFileExplorerWindow VerifyFolderExists(string folderName, bool expected = true);

        /// <summary>
        /// Verifies if path given as array of folder names exists
        /// </summary>
        /// <param name="path">Path given as folder names array</param>
        /// <returns>File Explorer window</returns>
        IFileExplorerWindow VerifyPathExists(params string[] path);

        /// <summary>
        /// Closes current File Explorer window
        /// </summary>
        void CloseWindow();

        /// <summary>
        /// Verifies amount of files and folders visible in folder
        /// </summary>
        /// <param name="expected">Expected amount of files and folders</param>
        /// <returns>File Explorer window</returns>
        IFileExplorerWindow VerifyFilesAmount(int expected);

        /// <summary>
        /// Verifies if files or folders have unique names
        /// </summary>
        /// <returns>File Explorer window</returns>
        IFileExplorerWindow VerifyFilesUnique();
    }
}