﻿namespace VersionVerification.Core.Views.Desktop
{
    public interface IDesktop
    {
        /// <summary>
        /// Verifies if specified path exists by searching and opening folders one by one
        /// </summary>
        /// <param name="path">Path to folder or file as string</param>
        /// <param name="isFilePath">Determines if there is a file extension in the path</param>
        /// <returns>Desktop window</returns>
        IDesktop VerifyPathExists(string path, bool isFilePath = true);
        
        /// <summary>
        /// Verifies if shortcut with specified name exists on desktop
        /// </summary>
        /// <param name="name">Name under the shortcut icon</param>
        /// <param name="expected">Determines if shortcut should exist or not</param>
        /// <returns>Desktop window</returns>
        IDesktop VerifyShortcutExists(string name, bool expected = true);
        
        /// <summary>
        /// Verifies if specified shortcut exists in start menu folder
        /// </summary>
        /// <param name="path">Path to the shortcut in start menu folder</param>
        /// <returns>Desktop window</returns>
        IDesktop CheckStartMenu(params string[] path);
    }
}
