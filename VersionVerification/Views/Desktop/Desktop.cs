﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.ExtensionMethods;
using VersionVerification.Core.Parsers;
using VersionVerification.Core.Properties;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Settings.Properties;

namespace VersionVerification.Core.Views.Desktop
{
    public class Desktop : IDesktop, IStartupView
    {
        public WinWindow ProgramManager
        {
            get
            {
                if (this.programManager == null)
                {
                    this.programManager = new WinWindow();
                    this.programManager.SearchProperties.Add(UITestControl.PropertyNames.Name, Resources.ProgramManager);
                    this.programManager.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }
                return this.programManager;
            }
        }

        public WinList DesktopList
        {
            get
            {
                if (this.desktopList == null)
                {
                    this.desktopList = new WinList(ProgramManager);
                    this.desktopList.SearchProperties.Add(UITestControl.PropertyNames.Name, "Desktop");
                    this.desktopList.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "List");
                }
                return this.desktopList;
            }
        }

        private WinWindow programManager;
        private WinList desktopList;

        public IDesktop VerifyPathExists(string path, bool isFilePath = true)
        {
            var pathParser = GetPathParser(isFilePath);

            var root = Path.GetPathRoot(path);
           ViewFactory.CreateView<FileExplorerWindow>(root)
                .VerifyPathExists(pathParser.ParsePath(path).ToArray())
                .CloseWindow();

            return this;
        }

        private static IPathParser GetPathParser(bool isFilePath) =>
              isFilePath ? (IPathParser)new FilePathParser() : new FolderPathParser();

        public IDesktop VerifyShortcutExists(string name, bool expected = true)
        {
            var shortcut = GetShortcut(name);
            var exists = shortcut != null && shortcut.Exists;
            var errorMessage = expected
                ? $"Shortcut {name} doesn't exist, but it should"
                : $"Shortcut {name} exists, but it shouldn't";

            Assert.AreEqual(expected, exists, errorMessage);

            return this;
        }

        private WinListItem GetShortcut(string name) => DesktopList.Items.FindControlByName(name) as WinListItem;

        public IDesktop CheckStartMenu(params string[] path)
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.StartMenuPath)
                .VerifyPathExists(path)
                .CloseWindow();

            return this;
        }
    }
}
