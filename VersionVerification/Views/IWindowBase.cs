﻿namespace VersionVerification.Core.Views
{
    public interface IWindowBase
    {
        /// <summary>
        /// Closes current window
        /// </summary>
        void CloseWindow();
    }
}