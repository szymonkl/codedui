﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.ExtensionMethods;
using VersionVerification.Core.Parsers;

namespace VersionVerification.Core.Views.ProgramsAndFeatures
{
    public class ProgramAndFeaturesWindow : WindowBase, IStartupView, IProgramAndFeaturesWindow
    {
        public override WinWindow Window
        {
            get
            {
                if (this.programsAndFeatures == null)
                {
                    this.programsAndFeatures = new WinWindow();
                    this.programsAndFeatures.SearchProperties.Add(UITestControl.PropertyNames.Name, Properties.Resources.ProgramsAndFeautures);
                    this.programsAndFeatures.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");

                }
                
                this.programsAndFeatures.SetFocus();
                return this.programsAndFeatures;
            }
        }

        public WinWindow FolderViewWindow
        {
            get
            {
                if (this.folderViewWindow == null)
                {
                    this.folderViewWindow = new WinWindow(Window);
                    this.folderViewWindow.SearchProperties.Add(WinWindow.PropertyNames.AccessibleName, Properties.Resources.FolderView);
                    this.folderViewWindow.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }

                return this.folderViewWindow;
            }
            
        }

        public WinList FolderView
        {
            get
            {
                if (this.folderView == null)
                {
                    this.folderView = new WinList(FolderViewWindow);
                    this.folderView.SearchProperties.Add(UITestControl.PropertyNames.Name, Properties.Resources.FolderView);
                    this.folderView.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "List");
                }

                this.folderView.WaitForControlReady(10000);
                return this.folderView;
            }
            
        }

        public WinScrollBar VerticalScrollBar
        {
            get
            {
                if (this.verticalScrollbar == null)
                {
                    this.verticalScrollbar = new WinScrollBar(FolderViewWindow);
                    this.verticalScrollbar.SearchProperties.Add(UITestControl.PropertyNames.Name, "Vertical");
                    this.verticalScrollbar.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "ScrollBar");
                }

                return this.verticalScrollbar;
            }
            
        }

        private WinList folderView;
        private WinWindow programsAndFeatures;
        private readonly IComponentInfoParser componentInfoParser = new AccessibleDescriptionParser();
        private WinWindow folderViewWindow;
        private WinScrollBar verticalScrollbar;

        public ProgramAndFeaturesWindow()
        {
            StartProcess();
        }

        private static void StartProcess()
        {
            var process = new Process();
            var processInfo = new ProcessStartInfo("appwiz.cpl");
            process.StartInfo = processInfo;
            process.Start();
        }

        public IProgramAndFeaturesWindow VerifyComponentVersion(string componentName, string version)
        {
            Assert.AreEqual(version, GetComponentVersion(componentName));

            return this;
        }

        private string GetComponentVersion(string componentName) => 
            GetComponentInfo(componentName)[Properties.Resources.Version];
        
        private Dictionary<string, string> GetComponentInfo(string componentName) =>
            this.componentInfoParser.Parse(FindComponent(componentName).AccessibleDescription);

        private WinListItem FindComponent(string componentName)
        {
            return FolderView.FindItem(componentName, HasVerticalScrollBar);
        }

        private bool HasVerticalScrollBar => VerticalScrollBar.TryFind();

        public IProgramAndFeaturesWindow VerifyComponentDistributor(string componentName, string distributor)
        {
            Assert.AreEqual(distributor, GetComponentPublisher(componentName));

            return this;
        }

        private string GetComponentPublisher(string componentName)
        {
            return GetComponentInfo(componentName)[Properties.Resources.Publisher];
        }
    }
}