﻿namespace VersionVerification.Core.Views.ProgramsAndFeatures
{
    public interface IProgramAndFeaturesWindow
    {
        /// <summary>
        /// Verifies version of installed component
        /// </summary>
        /// <param name="componentName">Name of the component</param>
        /// <param name="version">Expected version of the component</param>
        /// <returns>Programs and Features window</returns>
        IProgramAndFeaturesWindow VerifyComponentVersion(string componentName, string version);

        /// <summary>
        /// Verifies distributor of installed component
        /// </summary>
        /// <param name="componentName">Name of the component</param>
        /// <param name="distributor">Expected distributor of the component</param>
        /// <returns>Programs and Features window</returns>
        IProgramAndFeaturesWindow VerifyComponentDistributor(string componentName, string distributor);

        /// <summary>
        /// Closes current Programs and Features window
        /// </summary>
        void CloseWindow();
    }
}