﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;

namespace VersionVerification.Core.Views
{
    public class WindowBase : IWindowBase
    {
        public virtual WinWindow Window { get; set; }
        
        public virtual void CloseWindow()
        {
            Keyboard.SendKeys(Window, "%{F4}");
        }
    }
}