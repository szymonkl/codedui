﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Common;
using VersionVerification.Core.Views.Applications.SplashScreens;
using VersionVerification.Settings.Properties;

namespace VersionVerification.Core.Views.Applications
{
    public class DeviceProgrammerWindow : ApplicationWindowBase, IVerifyWindowTitle<DeviceProgrammerWindow>
    {
        protected override WpfWindow Window
        {
            get
            {
                if (this.window == null)
                {
                    this.window = new WpfWindow();
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.Name, Versions.Default.DPWindowName, PropertyExpressionOperator.Contains);
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                    this.window.WindowTitles.Add("RM Device Programmer Main Window");
                }
                return this.window;
            }
        }

        private WpfWindow window;
        private readonly IVerifySplashScreen splashScreen = new DeviceProgrammerSplashScreen();

        public DeviceProgrammerWindow()
        {
            Helpers.ProcessHelper.Start(Helpers.Paths.DeviceProgrammer);
        }

        public DeviceProgrammerWindow VerifySplashScreenVersion(string expected)
        {
            this.splashScreen.VerifySplashScreenVersion(expected);
            return this;
        }

        public DeviceProgrammerWindow VerifySplashScreenCopyright(string expected)
        {
            this.splashScreen.VerifySplashScreenCopyright(expected);
            return this;
        }

        public DeviceProgrammerWindow VerifyWindowTitle(string expected)
        {
            Assert.AreEqual(expected, Window.Name);
            return this;
        }
    }
}