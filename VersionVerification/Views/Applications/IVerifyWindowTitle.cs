﻿namespace VersionVerification.Core.Views.Applications
{
    public interface IVerifyWindowTitle<out T> where T: ApplicationWindowBase
    {
        /// <summary>
        /// Verifies version included in window title
        /// </summary>
        /// <param name="expected">Expected window title</param>
        /// <returns>Current window</returns>
        T VerifyWindowTitle(string expected);
    }
}
