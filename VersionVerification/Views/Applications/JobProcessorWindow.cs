﻿using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Common;
using VersionVerification.Settings.Properties;

namespace VersionVerification.Core.Views.Applications
{
    public class JobProcessorWindow : ApplicationWindowBase, IVerifyWindowTitle<JobProcessorWindow>
    {
        protected override WpfWindow Window
        {
            get
            {
                if (this.window == null)
                {
                    this.window = new WpfWindow();
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.Name, Versions.Default.JPWindowName);
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }
                return this.window;
            }
        }

        protected WpfText WindowTitle
        {
            get
            {
                if (this.windowTitle == null)
                {
                    this.windowTitle = new WpfText(Window);
                    this.windowTitle.SearchProperties.Add(WpfControl.PropertyNames.AutomationId, "SettingTitle");
                    this.windowTitle.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.windowTitle;
            }
        }

        private readonly ApplicationUnderTest applicationUnderTest;
        private WpfWindow window;
        private WpfText windowTitle;

        public JobProcessorWindow()
        {
            var processStartInfo = new ProcessStartInfo(Helpers.Paths.JobProcessor);
            var process = new Process { StartInfo = processStartInfo };
            process.Start();
            this.applicationUnderTest = ApplicationUnderTest.FromProcess(process);
        }

        public JobProcessorWindow VerifyWindowTitle(string expected)
        {
            Assert.AreEqual(expected, WindowTitle.DisplayText);
            return this;
        }

        public void CloseApplicationWindow()
        {
            this.applicationUnderTest.Close();
        }
    }
}