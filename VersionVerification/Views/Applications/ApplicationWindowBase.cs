﻿using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;

namespace VersionVerification.Core.Views.Applications
{
    public class ApplicationWindowBase : IApplicationView
    {
        protected virtual WpfWindow Window { get; }
       
    }
}
