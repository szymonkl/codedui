﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Common;
using VersionVerification.Core.Tests.Views.Applications.SplashScreens;
using VersionVerification.Core.Views.Applications.SplashScreens;
using VersionVerification.Settings.Properties;

namespace VersionVerification.Core.Views.Applications
{
    public class ServerUtilityWindow : ApplicationWindowBase, IVerifyWindowTitle<ServerUtilityWindow>
    {
        protected override WpfWindow Window
        {
            get
            {
                if (this.window == null)
                {
                    this.window = new WpfWindow();
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.Name, Versions.Default.RMServerUtilityFullName);
                    this.window.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }
                return this.window;
            }
        }

        protected WpfText WindowTitle
        {
            get
            {
                if (this.windowTitle == null)
                {
                    this.windowTitle = new WpfText(Window);
                    this.windowTitle.SearchProperties.Add(WpfControl.PropertyNames.AutomationId, "RMSystemHeader");
                    this.windowTitle.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.windowTitle;
            }
        }

        private WpfWindow window;
        private WpfText windowTitle;
        private readonly IVerifySplashScreen splashScreen = new ServerUtilitySplashScreen();
        
        public ServerUtilityWindow()
        {
            Helpers.ProcessHelper.Start(Helpers.Paths.ServerUtility);
        }

        public ServerUtilityWindow VerifySplashScreenVersion(string expected)
        {
            this.splashScreen.VerifySplashScreenVersion(expected);
            return this;
        }

        public ServerUtilityWindow VerifySplashScreenCopyright(string expected)
        {
            this.splashScreen.VerifySplashScreenCopyright(expected);
            return this;
        }

        public ServerUtilityWindow VerifyWindowTitle(string expected)
        {
            if (Window.WaitForControlExist(90000))
            {
                Window.SetFocus();
                var displayText = WindowTitle.DisplayText;
                Assert.IsTrue(displayText.Contains(expected),
                    $"Expected version in window title was {expected}, but found {displayText.Replace("RM System Version: ", string.Empty)}");
            }
            else
            {
                Assert.Fail("Cannot find RM Server Utility Window");
            }

            return this;
        }
    }
}