﻿using VersionVerification.Core.Common;
using VersionVerification.Core.Tests.Views.Applications.SplashScreens;
using VersionVerification.Core.Views.Applications.SplashScreens;

namespace VersionVerification.Core.Views.Applications
{
    public class TunerWindow : ApplicationWindowBase
    {
        private readonly IVerifySplashScreen splashScreen = new TunerSplashScreen();

        public TunerWindow()
        {
            Helpers.ProcessHelper.Start(Helpers.Paths.Tuner);
        }

        public TunerWindow VerifySplashScreenVersion(string expected)
        {
            this.splashScreen.VerifySplashScreenVersion(expected);
            return this;
        }

        public TunerWindow VerifySplashScreenCopyright(string expected)
        {
            this.splashScreen.VerifySplashScreenCopyright(expected);
            return this;
        }
    }
}