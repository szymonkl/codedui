﻿using VersionVerification.Core.Common;
using VersionVerification.Core.Views.Applications.SplashScreens;

namespace VersionVerification.Core.Views.Applications
{
    public class CPSWindow : ApplicationWindowBase
    {
        private readonly IVerifySplashScreen splashScreen = new CPSSplashScreen();

        public CPSWindow()
        {
            Helpers.ProcessHelper.Start(Helpers.Paths.CPS);
        }

        public CPSWindow VerifySplashScreenVersion(string expected)
        {
            this.splashScreen.VerifySplashScreenVersion(expected);
            return this;
        }

        public CPSWindow VerifySplashScreenCopyright(string expected)
        {
            this.splashScreen.VerifySplashScreenCopyright(expected);
            return this;
        }

    }
}