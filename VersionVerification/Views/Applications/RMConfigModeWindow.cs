﻿using VersionVerification.Core.Common;
using VersionVerification.Core.Views.Applications.SplashScreens;

namespace VersionVerification.Core.Views.Applications
{
    public class RMConfigModeWindow : ApplicationWindowBase
    {
        private readonly IVerifySplashScreen splashScreen = new RMConfigModeSplashScreen();

        public RMConfigModeWindow()
        {
            Helpers.ProcessHelper.Start(Helpers.Paths.RMConfigMode);
        }

        public RMConfigModeWindow VerifySplashScreenVersion(string expected)
        {
            this.splashScreen.VerifySplashScreenVersion(expected);
            return this;
        }

        public RMConfigModeWindow VerifySplashScreenCopyright(string expected)
        {
            this.splashScreen.VerifySplashScreenCopyright(expected);
            return this;
        }

    }
}