﻿namespace VersionVerification.Core.Views.Applications.SplashScreens
{
    public interface IVerifySplashScreen
    {
        /// <summary>
        /// Verifies version displayed on application splash screen
        /// </summary>
        /// <param name="expected">Expected version</param>
        void VerifySplashScreenVersion(string expected);

        /// <summary>
        /// Verifies copyright info displayed on application splash screen
        /// </summary>
        /// <param name="expected">Expected copyright info</param>
        void VerifySplashScreenCopyright(string expected);
    }
}
