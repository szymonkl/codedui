﻿using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VersionVerification.Core.Views.Applications.SplashScreens
{
    public class CPSSplashScreen : IVerifySplashScreen
    {
        protected WinWindow SplashScreenWindow
        {
            get
            {
                if (this.splashScreenWindow == null)
                {
                    this.splashScreenWindow = new WinWindow();
                    this.splashScreenWindow.SearchProperties.Add(UITestControl.PropertyNames.Name, "SplashScreen2");
                    this.splashScreenWindow.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }
                return this.splashScreenWindow;
            }
        }

        protected WinWindow Version
        {
            get
            {
                if (this.version == null)
                {
                    this.version = new WinWindow(SplashScreenWindow);
                    this.version.SearchProperties.Add(WinControl.PropertyNames.ControlName, "versionLabel");
                    this.version.SearchConfigurations.Add(SearchConfiguration.DisambiguateChild);
                }
                return this.version;
            }
        }

        protected WinText VersionText
        {
            get
            {
                if (this.versionText == null)
                {
                    this.versionText = new WinText(Version);
                    this.versionText.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.versionText;

            }
        }

        protected WinWindow Copyright
        {
            get
            {
                if (this.copyright == null)
                {
                    this.copyright = new WinWindow(SplashScreenWindow);
                    this.copyright.SearchProperties.Add(WinControl.PropertyNames.ControlName, "label2");
                    this.copyright.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                    this.copyright.SearchConfigurations.Add(SearchConfiguration.DisambiguateChild);
                }
                return this.copyright;
            }
        }

        protected WinText CopyrightText
        {
            get
            {
                if (this.copyrightText == null)
                {
                    this.copyrightText = new WinText(Copyright);
                    this.copyrightText.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.copyrightText;
            }
        }
        
        private WinWindow splashScreenWindow;
        private WinWindow version;
        private WinText versionText;
        private WinWindow copyright;
        private WinText copyrightText;

        public void VerifySplashScreenVersion(string expected)
        {
            Assert.AreEqual(expected, VersionText.DisplayText);
        }

        public void VerifySplashScreenCopyright(string expected)
        {
            Assert.AreEqual(expected, CopyrightText.DisplayText);
        }
    }
}