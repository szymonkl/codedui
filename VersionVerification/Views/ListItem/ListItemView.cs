﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using VersionVerification.Core.Views.FileProperties;

namespace VersionVerification.Core.Views.ListItem
{
    public class ListItemView : IListItemView
    {
        private readonly WinListItem listItem;

        public ListItemView(WinListItem listItem)
        {
            this.listItem = listItem;
        }
        
        public IFilePropertiesWindow OpenPropertiesWindow()
        {
            Keyboard.SendKeys(this.listItem, "%{ENTER}");
            return new FilePropertiesWindow();
        }
    }
}