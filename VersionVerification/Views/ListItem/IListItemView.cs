﻿using VersionVerification.Core.Views.FileProperties;

namespace VersionVerification.Core.Views.ListItem
{
    public interface IListItemView
    {
        /// <summary>
        /// Opens properties of the selected file
        /// </summary>
        /// <returns>File Properties window</returns>
        IFilePropertiesWindow OpenPropertiesWindow();
    }
}