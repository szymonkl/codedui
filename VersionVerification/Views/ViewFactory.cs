﻿using System;
using VersionVerification.Core.Views.Applications;

namespace VersionVerification.Core.Views
{
    public class ViewFactory
    {
        public static T CreateView<T>() where T : IStartupView, new()
        {
            return new T();
        }

        public static T CreateView<T>(string name) where T : IStartupView, new()
        {
            var instance = Activator.CreateInstance(typeof(T), name);
            return (T)instance;
        }

        public static T RunApplication<T>() where T : IApplicationView, new ()
        {
            return new T();
        }
    }
}
