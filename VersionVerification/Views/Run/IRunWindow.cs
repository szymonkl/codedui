﻿using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.ProgramsAndFeatures;

namespace VersionVerification.Core.Views.Run
{
    public interface IRunWindow
    {
        /// <summary>
        /// Opens Program and Features window
        /// </summary>
        /// <returns>Programs and Features window</returns>
        IProgramAndFeaturesWindow OpenProgramsAndFeaturesWindow();

        /// <summary>
        /// Opens File Explorer window on specified path
        /// </summary>
        /// <param name="path">Path to be opend given as a string</param>
        /// <returns>File Explorer window</returns>
        IFileExplorerWindow OpenFileExplorerWindow(string path);
    }
}