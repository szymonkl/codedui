﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.ProgramsAndFeatures;

namespace VersionVerification.Core.Views.Run
{
    public class RunWindow : IRunWindow
    {
        public WinWindow Run
        {
            get
            {
                if (_run == null)
                {
                    _run = new WinWindow();
                    _run.SearchProperties.Add(UITestControl.PropertyNames.Name, Properties.Resources.Run);
                    _run.SearchProperties.Add(UITestControl.PropertyNames.ClassName, "#32770");
                    _run.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }
                return _run;
            }
        }

        public WinEdit OpenEdit
        {
            get
            {
                if (_openEdit == null)
                {
                    _openEdit = new WinEdit(Run);
                    _openEdit.SearchProperties.Add(UITestControl.PropertyNames.Name, Properties.Resources.Open);
                    _openEdit.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Edit");
                }
                return _openEdit;
            }
        }

        public WinButton OkButton
        {
            get
            {
                if (_okButton == null)
                {
                    _okButton = new WinButton(Run);
                    _okButton.SearchProperties.Add(UITestControl.PropertyNames.Name, Properties.Resources.Ok);
                    _okButton.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Button");

                }
                return _okButton;
            }
        }

        private WinWindow _run;
        private WinEdit _openEdit;
        private WinButton _okButton;
        private const string AddRemoveProgramsPath = "control appwiz.cpl";

        public IProgramAndFeaturesWindow OpenProgramsAndFeaturesWindow()
        {
            OpenEdit.Text = AddRemoveProgramsPath;
            Mouse.Click(OkButton);
            return new ProgramAndFeaturesWindow();
        }

        public IFileExplorerWindow OpenFileExplorerWindow(string path)
        {
            OpenEdit.Text = path;
            Mouse.Click(OkButton);
            var windowName = path.Split(new[] {@"\"}, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
            return new FileExplorerWindow(windowName);
        }
    }
}