﻿using System.Diagnostics;
using System.IO;
using VersionVerification.Settings.Properties;

namespace VersionVerification.Core.Common
{
    public class Helpers
    {
        public class Paths
        {
            public static string CPS => Path.Combine(Versions.Default.Version_CPSPath, $"{Versions.Default.CPSName}.exe");
            public static string RMConfigMode => Path.Combine(Versions.Default.RMCPath, $"{Versions.Default.RMCName_Shell}.exe");
            public static string DeviceProgrammer => Path.Combine(Versions.Default.DPPath, $"{Versions.Default.DPName}.exe");
            public static string JobProcessor => Path.Combine(Versions.Default.JPPath, $"{Versions.Default.JPName}.exe");
            public static string ServerUtility => Path.Combine(Versions.Default.RMServerUtilityPath, $"{Versions.Default.RMServerUtilityName}.exe");
            public static string Tuner => Path.Combine(Versions.Default.Version_TunerPath, $"{Versions.Default.TunerName}.exe");
        }

        public class ProcessHelper
        {
            public static void Start(string path)
            {
                var processStartInfo = new ProcessStartInfo(path);
                var process = new Process { StartInfo = processStartInfo };
                process.Start();
            }
        }
    }
}
