﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using VersionVerification.Core.ExtensionMethods;

namespace VersionVerification.Core.Parsers
{
    public class FilePathParser : IPathParser
    {
        public IEnumerable<string> ParsePath(string path)
        {
            var splitPath = path.Split(new[] { @"\" }, StringSplitOptions.None);
            var fileName = Path.GetFileNameWithoutExtension(path);
            var pathList = splitPath.ToList().RemoveFirstAndLast();
            pathList.Add(fileName);
            return pathList;
        }
    }
}
