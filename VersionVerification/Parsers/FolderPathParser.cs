﻿using System;
using System.Collections.Generic;
using System.Linq;
using VersionVerification.Core.ExtensionMethods;

namespace VersionVerification.Core.Parsers
{
    public class FolderPathParser  : IPathParser
    {
        public IEnumerable<string> ParsePath(string path)
        {
            var splitPath = path.Split(new[] { @"\" }, StringSplitOptions.None);
            return splitPath.ToList().RemoveFirst();
        }
    }
}