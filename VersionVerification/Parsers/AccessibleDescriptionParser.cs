﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using VersionVerification.Core.Properties;

namespace VersionVerification.Core.Parsers
{
    public class AccessibleDescriptionParser : IComponentInfoParser
    {
        public Dictionary<string, string> Parse(string versionInfo)
        {
            var keys = new List<string> { Resources.Publisher, Resources.InstalledOn, Resources.Size, Resources.Version };

            var dictionary = keys.ToDictionary(k => k, v => string.Empty);


            foreach (var prefix in keys)
            {
                foreach (var postfix in keys)
                {
                    var match = Regex.Match(versionInfo, $"(?<={prefix}: )(.*)(?=(,|;) {postfix})");
                    if (match.Success)
                    {
                        if (!keys.Any(k => match.Value.Contains(k)))
                            dictionary[prefix] = match.Value;
                    }
                }
            }
            
            HandlingLastKey(versionInfo, dictionary);

            return dictionary;
        }

        private static void HandlingLastKey(string versionInfo, Dictionary<string, string> dictionary)
        {
            var emptyKeys = dictionary.Keys.Where(k => dictionary[k] == string.Empty);
            var lastKey = emptyKeys.FirstOrDefault(k => versionInfo.Contains(k));
            if (lastKey != null)
            {
                var split = versionInfo.Split(new[] {lastKey + ": "}, StringSplitOptions.None);
                if (split.Length == 2)
                {
                    dictionary[lastKey] = split[1];
                }
            }
        }
    }
}
