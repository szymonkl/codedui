﻿using System.Collections.Generic;

namespace VersionVerification.Core.Parsers
{
    public interface IComponentInfoParser
    {
        /// <summary>
        /// Parses information about component into dictionary
        /// </summary>
        /// <param name="versionInfo">Source string to be parsed</param>
        /// <returns>Dictionary of parsed strings</returns>
        Dictionary<string, string> Parse(string versionInfo);
    }
}