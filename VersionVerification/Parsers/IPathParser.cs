﻿using System.Collections.Generic;

namespace VersionVerification.Core.Parsers
{
    public interface IPathParser
    {
        /// <summary>
        /// Parses path into string enumerable
        /// </summary>
        /// <param name="path">Path given as a string</param>
        /// <returns>IEnumerable of strings</returns>
        IEnumerable<string> ParsePath(string path);
    }
}
