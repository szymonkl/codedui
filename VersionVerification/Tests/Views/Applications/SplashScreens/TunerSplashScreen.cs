﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Views.Applications.SplashScreens;

namespace VersionVerification.Core.Tests.Views.Applications.SplashScreens
{
    public class TunerSplashScreen : IVerifySplashScreen
    {
        protected WpfWindow SplashScreenWindow
        {
            get
            {
                if (this.splashScreenWindow == null)
                {
                    this.splashScreenWindow = new WpfWindow();
                    this.splashScreenWindow.SearchProperties.Add(UITestControl.PropertyNames.Name, "Tuner Splash Screen");
                    this.splashScreenWindow.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Window");
                }
                return this.splashScreenWindow;
            }
        }

        protected WpfText Version
        {
            get
            {
                if (this.version == null)
                {
                    this.version = new WpfText(SplashScreenWindow);
                    this.version.SearchProperties.Add(WpfControl.PropertyNames.AutomationId, "Version_Textblock");
                    this.version.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.version;
            }
        }

        protected WpfText VersionText
        {
            get
            {
                if (this.versionText == null)
                {
                    this.versionText = new WpfText(Version);
                    this.versionText.SearchProperties.Add(UITestControl.PropertyNames.Name, "Version", PropertyExpressionOperator.Contains);
                    this.versionText.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.version;
            }
        }

        protected WpfText Copyright
        {
            get
            {
                if (this.copyright == null)
                {
                    this.copyright = new WpfText(SplashScreenWindow);
                    this.copyright.SearchProperties.Add(WpfControl.PropertyNames.AutomationId, "_2008_MOTOROLA");
                    this.copyright.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.copyright;
            }
        }

        protected WpfText CopyrightText
        {
            get
            {
                if (this.copyrightText == null)
                {
                    this.copyrightText = new WpfText(Copyright);
                    this.copyrightText.SearchProperties.Add(UITestControl.PropertyNames.Name, "MOTOROLA SOLUTIONS, INC. ALL RIGHTS RESERVED.", PropertyExpressionOperator.Contains);
                    this.copyrightText.SearchProperties.Add(UITestControl.PropertyNames.ControlType, "Text");
                }
                return this.copyrightText;
            }
        }

        private WpfWindow splashScreenWindow;
        private WpfText version;
        private WpfText versionText;
        private WpfText copyright;
        private WpfText copyrightText;

        public void VerifySplashScreenVersion(string expected)
        {
            Assert.AreEqual(expected, VersionText.DisplayText);
        }

        public void VerifySplashScreenCopyright(string expected)
        {
            Assert.AreEqual(expected, CopyrightText.DisplayText);
        }
    }
}