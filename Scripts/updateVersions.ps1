﻿$appConfig = [xml](Get-Content "C:\tmp\app.config");
$node = $appConfig.configuration.userSettings."VersionTests.Tests.ASTRO.Versions".setting | where {$_.name -eq 'Version_CPSFile'};

$version = [version]($node.value);

$newVersion = New-Object -TypeName System.Version -ArgumentList $version.Major, $version.Minor, $version.Build, ($version.Revision + 1); 

$node.value = [string]$newVersion;
$appConfig.Save("C:\tmp\app.config");