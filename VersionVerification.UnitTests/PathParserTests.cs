﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Parsers;

namespace VersionVerification.UnitTests
{
    [TestClass]
    public class PathParserTests
    {
        [TestMethod]
        public void FilePathParserTest()
        {
            var expected = new[]
            {
                "Program Files(x86)",
                "Microsoft Visual Studio",
                "2017",
                "Enterprise",
                "Common7",
                "IDE",
                "mtm"
            };

            IPathParser parser = new FilePathParser();
            var actual = parser.ParsePath(@"C:\Program Files(x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\mtm.exe");
            CollectionAssert.AreEqual(expected, actual.ToArray());
        }

        [TestMethod]
        public void FolderPathParserTest()
        {
            var expected = new[]
            {
                "Program Files(x86)",
                "Microsoft Visual Studio",
                "2017",
                "Enterprise",
                "Common7",
                "IDE"
            };

            IPathParser parser = new FolderPathParser();
            var actual = parser.ParsePath(@"C:\Program Files(x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE");
            CollectionAssert.AreEqual(expected, actual.ToArray());
        }
    }
}
