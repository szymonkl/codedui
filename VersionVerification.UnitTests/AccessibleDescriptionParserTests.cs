﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Parsers;
using VersionVerification.Core.Properties;

namespace VersionVerification.UnitTests
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void AccessibleDescriptionParserTests_AllFieldsAvailable()
        {
            IComponentInfoParser parser = new AccessibleDescriptionParser();
            var dictionary = parser.Parse("Publisher: Google Inc., Installed On: ‎4/‎3/‎2017, Size: 353 MB, Version: 67.0.3396.87");
            Assert.AreEqual("Google Inc.", dictionary[Resources.Publisher]);
            Assert.AreEqual("‎4/‎3/‎2017", dictionary[Resources.InstalledOn]);
            Assert.AreEqual("353 MB", dictionary[Resources.Size]);
            Assert.AreEqual("67.0.3396.87", dictionary[Resources.Version]);
        }

        [TestMethod]
        public void AccessibleDescriptionParserTests_SizeVersionNotAvailable()
        {
            IComponentInfoParser parser = new AccessibleDescriptionParser();
            var dictionary = parser.Parse("Publisher: Microsoft Corporation, Installed On: ‎5/‎18/‎2018");
            Assert.AreEqual("Microsoft Corporation", dictionary[Resources.Publisher]);
            Assert.AreEqual("‎5/‎18/‎2018", dictionary[Resources.InstalledOn]);
            Assert.AreEqual(string.Empty, dictionary[Resources.Size]);
            Assert.AreEqual(string.Empty, dictionary[Resources.Version]);
        }

        [TestMethod]
        public void AccessibleDescriptionParserTests_SizeNotAvailable()
        {
            IComponentInfoParser parser = new AccessibleDescriptionParser();
            var dictionary = parser.Parse("Publisher: Microsoft Corporation, Installed On: ‎12/‎15/‎2017, Version: 1.16.1243.427");
            Assert.AreEqual("Microsoft Corporation", dictionary[Resources.Publisher]);
            Assert.AreEqual("‎12/‎15/‎2017", dictionary[Resources.InstalledOn]);
            Assert.AreEqual(string.Empty, dictionary[Resources.Size]);
            Assert.AreEqual("1.16.1243.427", dictionary[Resources.Version]);
        }

        [TestMethod]
        public void AccessibleDescriptionParserTests_VersionNotAvailable()
        {
            IComponentInfoParser parser = new AccessibleDescriptionParser();
            var dictionary = parser.Parse("Publisher: AOMEI Technology Co., Ltd., Installed On: ‎2/‎13/‎2018, Size: 206 MB");
            Assert.AreEqual("AOMEI Technology Co., Ltd.", dictionary[Resources.Publisher]);
            Assert.AreEqual("‎2/‎13/‎2018", dictionary[Resources.InstalledOn]);
            Assert.AreEqual("206 MB", dictionary[Resources.Size]);
            Assert.AreEqual(string.Empty, dictionary[Resources.Version]);
        }

        [TestMethod]
        public void AccessibleDescriptionParserTests_PublisherSizeVersionNotAvailable()
        {
            IComponentInfoParser parser = new AccessibleDescriptionParser();
            var dictionary = parser.Parse("Installed On: ‎5/‎18/‎2018");
            Assert.AreEqual(string.Empty, dictionary[Resources.Publisher]);
            Assert.AreEqual("‎5/‎18/‎2018", dictionary[Resources.InstalledOn]);
            Assert.AreEqual(string.Empty, dictionary[Resources.Size]);
            Assert.AreEqual(string.Empty, dictionary[Resources.Version]);
        }

        [TestMethod]
        public void AccessibleDescriptionTests_ChangedColumnOrder()
        {
            IComponentInfoParser parser = new AccessibleDescriptionParser();
            var dictionary = parser.Parse("Publisher: Google Inc., Version: 67.0.3396.87, Installed On: ‎4/‎3/‎2017, Size: 353 MB");
            Assert.AreEqual("Google Inc.", dictionary[Resources.Publisher]);
            Assert.AreEqual("‎4/‎3/‎2017", dictionary[Resources.InstalledOn]);
            Assert.AreEqual("353 MB", dictionary[Resources.Size]);
            Assert.AreEqual("67.0.3396.87", dictionary[Resources.Version]);
        }

        [TestMethod]
        public void AccessibleDescriptionTests_ChangedSeparator()
        {
            IComponentInfoParser parser = new AccessibleDescriptionParser();
            var dictionary = parser.Parse("Publisher: Google Inc.; Version: 67.0.3396.87; Installed On: ‎4/‎3/‎2017; Size: 353 MB");
            Assert.AreEqual("Google Inc.", dictionary[Resources.Publisher]);
            Assert.AreEqual("‎4/‎3/‎2017", dictionary[Resources.InstalledOn]);
            Assert.AreEqual("353 MB", dictionary[Resources.Size]);
            Assert.AreEqual("67.0.3396.87", dictionary[Resources.Version]);
        }


    }
}
