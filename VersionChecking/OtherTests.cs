﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class OtherTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckFolders()
        {
           ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .VerifyFolderExists(Versions.Default.DiscoveryServiceName)
                .VerifyFolderExists(Versions.Default.UpdaterService)
                .CloseWindow();
        }
    }
}
