﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.FileProperties.Tabs;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
        [CodedUITest]
        public class RMServerTests : VersionVerificationTestBase
        {
            [TestMethod]
            public void CheckExeFile()
            {
                ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.RMServerPath)
                    .SelectItem(Versions.Default.RMServerName)
                    .OpenPropertiesWindow()
                    .OpenPropertyTab<DetailsPropertyTab>()
                    .VerifyProductName(Versions.Default.RadioManagement_Motorola)
                    .VerifyProductVersion(Versions.Default.Version_RM)
                    .VerifyFileVersion(Versions.Default.Version_RM)
                    .VerifyCopyrightInfo(Versions.Default.Copyright_RM)
                    .CloseWindow();
            }
        }
    
}
