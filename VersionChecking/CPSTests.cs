﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.Desktop;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.FileProperties.Tabs;
using VersionVerification.Core.Views.ProgramsAndFeatures;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class CPSTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckExeFile()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.Version_CPSPath)
                .SelectItem(Versions.Default.CPSName)
                .OpenPropertiesWindow()
                .OpenPropertyTab<DetailsPropertyTab>()
                .VerifyProductName(Versions.Default.CPSFullName)
                .VerifyProductVersion(Versions.Default.Version_CPSProduct)
                .VerifyFileVersion(Versions.Default.Version_CPSFile)
                .VerifyCopyrightInfo(Versions.Default.Copyright_CPS)
                .CloseWindow();

        }

        [TestMethod]
        public void CheckStartMenu()
        {
            ViewFactory.CreateView<Desktop>()
                .CheckStartMenu(Versions.Default.Motorola, Versions.Default.Version_CPSName, Versions.Default.Version_CPSName_LongVersionNumber)
                .CheckStartMenu(Versions.Default.Motorola, Versions.Default.Version_CPSName, Versions.Default.Uninstall,
                    Versions.Default.Version_CPSUninstall);
        }

        [TestMethod]
        public void CheckDesktopShortcut()
        {
            ViewFactory.CreateView<Desktop>()
                .VerifyShortcutExists(Versions.Default.Version_CPSName);
        }

        [TestMethod]
        public void CheckFolder()
        {
           ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .VerifyFolderExists(Versions.Default.Version_CPSName)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckAddRemovePrograms()
        {
            ViewFactory.CreateView<ProgramAndFeaturesWindow>()
                .VerifyComponentVersion(Versions.Default.Version_CPSName_LongVersionNumber, Versions.Default.Version_CPSComponent)
                .VerifyComponentDistributor(Versions.Default.Version_CPSName_LongVersionNumber, Versions.Default.Copyright)
                .CloseWindow();
        }

    }
}
