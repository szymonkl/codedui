﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class RadioLanguagePacksTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckLanguagePacksAmount()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .OpenFolder(Versions.Default.Version_CPSName)
                .OpenFolder(Versions.Default.RadioLanguagePacks)
                .VerifyFilesAmount(7)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckLanguagePacksUnique()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .OpenFolder(Versions.Default.Version_CPSName)
                .OpenFolder(Versions.Default.RadioLanguagePacks)
                .VerifyFilesUnique()
                .CloseWindow();
        }

        [TestMethod]
        public void CheckLanguagePacksVersions()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .OpenFolder(Versions.Default.Version_CPSName)
                .OpenFolder(Versions.Default.RadioLanguagePacks)
                .VerifyFolderExists(
                    $"{Versions.Default.RadioLanguagePacks_Arabic}_{Versions.Default.Version_RadioLanguagePacks}.lpk")
                .VerifyFolderExists(
                    $"{Versions.Default.RadioLanguagePacks_ChineseTraditional}_{Versions.Default.Version_RadioLanguagePacks}.lpk")
                .VerifyFolderExists(
                    $"{Versions.Default.RadioLanguagePacks_French}_{Versions.Default.Version_RadioLanguagePacks}.lpk")
                .VerifyFolderExists(
                    $"{Versions.Default.RadioLanguagePacks_Hebrew}_{Versions.Default.Version_RadioLanguagePacks}.lpk")
                .VerifyFolderExists(
                    $"{Versions.Default.RadioLanguagePacks_Portuguese}_{Versions.Default.Version_RadioLanguagePacks}.lpk")
                .VerifyFolderExists(
                    $"{Versions.Default.RadioLanguagePacks_Russian}_{Versions.Default.Version_RadioLanguagePacks}.lpk")
                .VerifyFolderExists(
                    $"{Versions.Default.RadioLanguagePacks_Spanish}_{Versions.Default.Version_RadioLanguagePacks}.lpk")
                .CloseWindow();
        }
    }
}
