﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.Desktop;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.FileProperties.Tabs;
using VersionVerification.Core.Views.ProgramsAndFeatures;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class ASKTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckExeFile()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.Version_ASKPath)
                .SelectItem(Versions.Default.ASKName)
                .OpenPropertiesWindow()
                .OpenPropertyTab<DetailsPropertyTab>()
                .VerifyProductName(Versions.Default.ASKProductName)
                .VerifyProductVersion(Versions.Default.Version_ASKProduct)
                .VerifyFileVersion(Versions.Default.Version_ASKFile)
                .VerifyCopyrightInfo(Versions.Default.Copyright_ASK)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckStartMenu()
        {
           ViewFactory.CreateView<Desktop>()
                .CheckStartMenu(Versions.Default.Motorola, Versions.Default.ASKFullName,
                    Versions.Default.Version_ASKFullName);
        }

        [TestMethod]
        public void CheckDesktopShortcut()
        {
           ViewFactory.CreateView<Desktop>()
                .VerifyShortcutExists(Versions.Default.Version_ASKFullName);
        }

        [TestMethod]
        public void CheckFolder()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .VerifyFolderExists(Versions.Default.Version_ASKFullName)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckAddRemovePrograms()
        {
            ViewFactory.CreateView<ProgramAndFeaturesWindow>()
                .VerifyComponentVersion(Versions.Default.Version_ASKFullName_LongVersionNumber, Versions.Default.Version_ASKComponent)
                .VerifyComponentDistributor(Versions.Default.Version_ASKFullName_LongVersionNumber, Versions.Default.Distributor)
                .CloseWindow();
        }
    }
}
