﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.Applications;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class ApplicationsTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckDeviceProgrammerSplashScreenVersion()
        {
            ViewFactory.RunApplication<DeviceProgrammerWindow>()
                .VerifySplashScreenVersion($"Version: {Versions.Default.Version_RM}");
        }


        [TestMethod]
        public void CheckDeviceProgrammerSplashScreenCopyright()
        {
            ViewFactory.RunApplication<DeviceProgrammerWindow>()
                .VerifySplashScreenCopyright(Versions.Default.Copyright_RM_SplashScreen);
        }

        [TestMethod]
        public void CheckDeviceProgrammerWindowTitle()
        {
            ViewFactory.RunApplication<DeviceProgrammerWindow>()
                .VerifyWindowTitle($"{Versions.Default.DPWindowName}({Versions.Default.Version_RM})");
        }

        [TestMethod]
        public void CheckJobProcessorWindowTitle()
        {
            ViewFactory.RunApplication<JobProcessorWindow>()
                .VerifyWindowTitle($"{Versions.Default.MotorolaJPSettings}({Versions.Default.Version_RM})");
        }

        [TestMethod]
        public void CheckRMConfigSplashScreenVersion()
        {
            ViewFactory.RunApplication<RMConfigModeWindow>()
                .VerifySplashScreenVersion($"Version {Versions.Default.Version_RM}");
        }

        [TestMethod]
        public void CheckRMConfigSplashScreenCopyright()
        {
            ViewFactory.RunApplication<RMConfigModeWindow>()
                .VerifySplashScreenCopyright(Versions.Default.Copyright_RM_SplashScreen);
        }

        [TestMethod]
        public void CheckRMServerUtilityWindowTitle()
        {
            ViewFactory.RunApplication<ServerUtilityWindow>()
                .VerifyWindowTitle(Versions.Default.Version_RM);
        }

        [TestMethod]
        public void CheckRMServerUtilitySplashScreenVersion()
        {
            ViewFactory.RunApplication<ServerUtilityWindow>()
                .VerifySplashScreenVersion($"Version {Versions.Default.Version_RM}");
        }

        [TestMethod]
        public void CheckRMServerUtilityCopyright()
        {
            ViewFactory.RunApplication<ServerUtilityWindow>()
                .VerifySplashScreenCopyright(Versions.Default.Copyright_RM_SplashScreen);
        }

        [TestMethod]
        public void CheckCPSSplashScreenVersion()
        {
            ViewFactory.RunApplication<CPSWindow>()
                .VerifySplashScreenVersion(Versions.Default.Version_CPSProduct);
        }

        [TestMethod]
        public void CheckCPSCopyright()
        {
            ViewFactory.RunApplication<CPSWindow>()
                .VerifySplashScreenCopyright(Versions.Default.Copyright_CPS_SplashScreen);
        }

        [TestMethod]
        public void CheckTunerSplashScreenVersion()
        {
            ViewFactory.RunApplication<TunerWindow>()
                .VerifySplashScreenVersion($"Version: {Versions.Default.Version_TunerProduct}");
        }

        [TestMethod]
        public void CheckTunerCopyright()
        {
            ViewFactory.RunApplication<TunerWindow>()
                .VerifySplashScreenCopyright(Versions.Default.Copyright_Tuner_SplashScreen);
        }
    }
}
