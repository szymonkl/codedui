﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.Desktop;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.FileProperties.Tabs;
using VersionVerification.Core.Views.ProgramsAndFeatures;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class ARSTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckExeFile()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.ARSPath)
                .SelectItem(Versions.Default.ARSName)
                .OpenPropertiesWindow()
                .OpenPropertyTab<DetailsPropertyTab>()
                .VerifyProductName(Versions.Default.ARSName)
                .VerifyProductVersion(Versions.Default.Version_ARSProduct)
                .VerifyFileVersion(Versions.Default.Version_ARSFile)
                .VerifyCopyrightInfo(Versions.Default.Copyright_ARS)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckStartMenu()
        {
           ViewFactory.CreateView<Desktop>()
                .CheckStartMenu(Versions.Default.Motorola, Versions.Default.ARSFullName,
                    Versions.Default.ARSFullName);
        }

        [TestMethod]
        public void CheckDesktopShortcut()
        {
            ViewFactory.CreateView<Desktop>()
                .VerifyShortcutExists(Versions.Default.ARSFullName);
        }

        [TestMethod]
        public void CheckFolder()
        {
           ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .VerifyFolderExists(Versions.Default.ARSFullName)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckAddRemovePrograms()
        {
           ViewFactory.CreateView<ProgramAndFeaturesWindow>()
                .VerifyComponentVersion(Versions.Default.ARSFullName, Versions.Default.Version_ARSProduct)
                .VerifyComponentDistributor(Versions.Default.ARSFullName, Versions.Default.Copyright)
                .CloseWindow();
        }
    }
}
