﻿using System.IO;
using System.Linq;

namespace VersionChecking
{
    internal interface IDirectoryChecker
    {
        bool CheckDirectoryExists(string path);
        bool CheckDirectoryExists(string path, string version);

    }

    internal class DirectoryChecker : IDirectoryChecker
    {
        public bool CheckDirectoryExists(string path)
        {
            return Directory.Exists(path);
        }

        public bool CheckDirectoryExists(string path, string version)
        {
            if (Directory.Exists(path))
            {
                return true;
            }
            else
            {
                var pathWithoutDirectory = Path.GetDirectoryName(path);
                if (Directory.Exists(pathWithoutDirectory))
                {
                    var subDirectories = Directory.GetDirectories(pathWithoutDirectory);
                    var searchedDirectory = subDirectories.FirstOrDefault(s => s.StartsWith(Path.GetFileName(path)));

                }

                return false;

            }

            
        }
    }
}
