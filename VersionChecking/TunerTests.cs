﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.Desktop;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.FileProperties.Tabs;
using VersionVerification.Core.Views.ProgramsAndFeatures;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class TunerTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckExeFile()
        {
           ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.Version_TunerPath)
                .SelectItem(Versions.Default.TunerName)
                .OpenPropertiesWindow()
                .OpenPropertyTab<DetailsPropertyTab>()
                .VerifyProductName(Versions.Default.TunerFullName)
                .VerifyProductVersion(Versions.Default.Version_TunerProduct)
                .VerifyFileVersion(Versions.Default.Version_TunerFile)
                .VerifyCopyrightInfo(Versions.Default.Copyright_Tuner)
               .CloseWindow();

        }

        [TestMethod]
        public void CheckStartMenu()
        {
           ViewFactory.CreateView<Desktop>()
                .CheckStartMenu(Versions.Default.Motorola, Versions.Default.Version_TunerName, Versions.Default.Version_TunerFullName)
                .CheckStartMenu(Versions.Default.Motorola, Versions.Default.Version_TunerName, Versions.Default.Uninstall,
                    Versions.Default.Version_TunerUninstall);
        }

        [TestMethod]
        public void CheckDesktopShortcut()
        {
            ViewFactory.CreateView<Desktop>()
                .VerifyShortcutExists(Versions.Default.Version_TunerName);
        }

        [TestMethod]
        public void CheckFolder()
        {
           ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .VerifyFolderExists(Versions.Default.Version_TunerName)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckAddRemovePrograms()
        {
            ViewFactory.CreateView<ProgramAndFeaturesWindow>()
                .VerifyComponentVersion(Versions.Default.TunerName, Versions.Default.Version_TunerComponent)
                .VerifyComponentDistributor(Versions.Default.TunerName, Versions.Default.Distributor)
                .CloseWindow();
        }

    }

}
