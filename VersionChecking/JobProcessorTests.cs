﻿using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionVerification.Core.Engine;
using VersionVerification.Core.Views;
using VersionVerification.Core.Views.Desktop;
using VersionVerification.Core.Views.FileExplorer;
using VersionVerification.Core.Views.FileProperties.Tabs;
using VersionVerification.Core.Views.ProgramsAndFeatures;
using VersionVerification.Settings.Properties;

namespace VersionChecking
{
    [CodedUITest]
    public class JobProcessorTests : VersionVerificationTestBase
    {
        [TestMethod]
        public void CheckExeFile()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.JPPath)
                .SelectItem(Versions.Default.JPName)
                .OpenPropertiesWindow()
                .OpenPropertyTab<DetailsPropertyTab>()
                .VerifyProductName(Versions.Default.RadioManagement_Motorola)
                .VerifyProductVersion(Versions.Default.Version_RM)
                .VerifyFileVersion(Versions.Default.Version_RM)
                .VerifyCopyrightInfo(Versions.Default.Copyright_RM)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckStartMenu()
        {
            ViewFactory.CreateView<Desktop>()
                .CheckStartMenu(Versions.Default.Motorola, Versions.Default.JPFullName,
                    Versions.Default.JPFullName_Config);
        }

        [TestMethod]
        public void CheckFolder()
        {
            ViewFactory.CreateView<FileExplorerWindow>(Versions.Default.MotorolaPath)
                .VerifyFolderExists(Versions.Default.JPFullName)
                .CloseWindow();
        }

        [TestMethod]
        public void CheckAddRemovePrograms()
        {
            ViewFactory.CreateView<ProgramAndFeaturesWindow>()
                .VerifyComponentVersion(Versions.Default.JPFullName_Motorola, Versions.Default.Version_RM_Short)
                .VerifyComponentDistributor(Versions.Default.JPFullName_Motorola, Versions.Default.Distributor)
                .CloseWindow();
        }
    }
}
